import React from "react"
import { useStaticQuery, graphql } from "gatsby"

const Podcast = () => {
  const data = useStaticQuery(graphql`
    query Podcast {
      allFeedPodcastDeLaMeuf {
        edges {
          node {
            enclosure {
              url
              length
            }
            itunes {
              image
            }
          }
        }
      }
    }
  `)

  console.log('Podcast', data.allFeedPodcastDeLaMeuf.edges);

  return (
    <>
      {data.allFeedPodcastDeLaMeuf.edges.map((podcast, index) => {
        console.log('result =>', podcast);
        return (
          <div key={index}>
            <img src={podcast.node.itunes.image} style={{ width: '100px', height: '100px' }} alt="toto"/>
            <audio controls="controls">
              <source src={podcast.node.enclosure.url} type="audio/mp3" />
              {/* <track src={podcast.node.itunes.subtitle} kind="subtitles" srcLang="fr" label="francais"></track> */}
            </audio>
          </div>
        )
      })}
    </>
  )
}

export default Podcast

import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import { Card } from "antd"

const { Meta } = Card

const InstagramCard = () => {
  const data = useStaticQuery(graphql`
  query InstagramCard {
    allInstaNode {
      edges {
        node {
          id
          likes
          comments
          mediaType
          preview
          original
          timestamp
          caption
          thumbnails {
            src
            config_width
            config_height
          }
          dimensions {
            height
            width
          }
        }
      }
    }
  }
  `)

  const lastPostInsta = data.allInstaNode.edges[data.allInstaNode.edges.length - 1].node;
  return (
    <>
      <Card
        hoverable
        style={{ width: 240 }}
        cover={<img alt="insta" src={lastPostInsta.preview} />}
      >
        <p>{lastPostInsta.likes} Like</p>
        <Meta
          // title="Europe Street beat"
          description={lastPostInsta.caption}
        />
      </Card>
    </>
  )
}

export default InstagramCard

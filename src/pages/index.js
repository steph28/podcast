import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import InstaCard from "../components/instagramCard"
import Podcast from "../components/podcast"

import { Row, Col } from "antd"

import 'antd/dist/antd.css';

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <Row>
      <Col span={12}><InstaCard /></Col>
      <Col span={12}><Podcast /></Col>
    </Row>
    <Link to="/page-2/">Go to page 2</Link>&nbsp;
    <Link to="/instagram/">Instagram page</Link>
  </Layout>
)

export default IndexPage

import React from "react"
import { Link } from "gatsby"
import { useStaticQuery, graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const InstagramPage = () => {
  const data = useStaticQuery(graphql`
    query InstagramPage {
      allInstaNode {
        edges {
          node {
            id
            likes
            comments
            mediaType
            preview
            original
            timestamp
            caption
            thumbnails {
              src
              config_width
              config_height
            }
            dimensions {
              height
              width
            }
          }
        }
      }
    }
  `)

  return (
    <Layout>
      <SEO title="Instagram" />
      <h1>Hi from the Instagram page</h1>
      <p>Welcome to page Instagram</p>
      <div>
        <p>{data.allInstaNode.edges[data.allInstaNode.edges.length - 1].node.caption}</p>
        <img src={data.allInstaNode.edges[data.allInstaNode.edges.length - 1].node.preview} alt="toto" />
      </div>
      <Link to="/">Go back to the homepage</Link>
    </Layout>
  )
}

export default InstagramPage
